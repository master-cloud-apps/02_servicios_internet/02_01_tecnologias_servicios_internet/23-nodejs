let express = require('express')
// eslint-disable-next-line new-cap
let router = express.Router()

router.get('/', (req, res) => {
  res.render('index', {
    name: 'Luke'
  })
})

module.exports = router