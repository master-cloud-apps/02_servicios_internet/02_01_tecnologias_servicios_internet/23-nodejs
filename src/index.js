const processArray = (inputArray) =>
  inputArray.map((innerArray) => innerArray.filter((number) => number !== 0))

module.exports = processArray
